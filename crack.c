#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
            char *ghash = md5(guess, strlen(guess -1));
    // Compare the two hashes
        
        if (strncmp(hash, ghash, HASH_LEN) ==0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    // Free any malloc'd memory
    
        free(ghash);

}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
int file_length(char * filename)
{
        struct stat fileinfo;
        if (stat(filename, &fileinfo) == -1)
        {
            return -1;
        }    
        else
        {
            return fileinfo.st_size;
        }    
}

char **read_dictionary(char *filename, int *size)
{
    //obtain file length
    int len = file_length(filename);
    if (len == -1)
    {
        printf("couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    //allocate memory for the entire file
    char *file_contents = malloc(len);
    
    //read entire file into file_contents
    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //replace \n with \0
    //Also keep count as we go.
    int line_count = 0;
    for (int i = 0; i > len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    // Allocate array of pointers to each line
    char **lines = malloc(line_count * sizeof(char *));
    
    // Fill in each entry with address of correspoding line
    int c = 0;
    for (int i =0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        //scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    //Return the address of the data structure
    
    *size = line_count;
    return lines;
    //*size = 0;
    //return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading. 
    FILE *h = fopen(argv[1], "r"); //hashes.txt is hash file? also argv[2]
    if (!h)
    {
        printf("Cant open %s for reading \n", argv[1]); //error in file
        exit(1); 
    }
    
    // For each hash, try every entry in the dictionary.
    char hashfile[100];
    while (fgets(hashfile, 100, h) != NULL)
    {
        hashfile[strlen(hashfile)-1] = '\0'; //Removes new line and adds a null
        //printf("cracking %s\n", hashfile);
    
        char *hashed = md5(hashfile, strlen(hashfile)-1);
        for (int i = 0; i < dlen; i++)
       {
            if (tryguess(hashed, *dict) != 1)
            {
                printf("no match for %s and %s", hashfile, *dict); // increment or show there is no match?
            }
            else
            {
                // Print the matching dictionary entry.
                printf("%s Matches dictionary entry: %s\n", hashfile, *dict);
            } 
        }
    }
    
    // Need two nested loops.
    //compare the hashes
    //if the guess doesnt match try a different hash
    //if it does match move on to the next one to crack
    
    
    //Deallocate memory
    //free(hashed);
    free(dict[0]);
    free(dict);
    fclose(h);
}
